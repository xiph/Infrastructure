# OSUOSL's smtpd auditor

This is a script to audit the TLS config of mailservers.
It uses the cinc project's auditor packages, compatible
with chef, and a rule set downloaded from osuosl.org.

There's also a `docker.io/cincproject/auditor` image, but
it doesn't support aarch64, and of course doesn't have our
local default command line.

To build the image
(Subsitute `docker` for `podman` if you prefer that tool.)
```
podman build -t osuosl-auditor .
```


Invoke with bare arguments to check the default mailserver:
```
podman run --rm osuosl-auditor
```

Invoke against a different host or ports:
```
podman run --rm osuosl-auditor cinc-auditor exec \
  --input ssl_host=hostfish.xiph.org ssl_port=[25,465] -l info \
  https://github.com/osuosl/osuosl-baseline/archive/main.tar.gz
```
